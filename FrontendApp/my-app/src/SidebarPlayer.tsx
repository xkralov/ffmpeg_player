import React from 'react'
import axios from 'axios'
import './App.css'

interface SidebarProps {
    setTransmit: React.Dispatch<React.SetStateAction<boolean>>
    setVideoActive: React.Dispatch<React.SetStateAction<boolean>>
    inputActive: boolean
    setInputActive: React.Dispatch<React.SetStateAction<boolean>>
    decklinkActive: boolean
    setDecklinkActive: React.Dispatch<React.SetStateAction<boolean>>
}

export const SidebarPlayer = (
    {   
        setTransmit: setTransmit,
        setVideoActive: setVideoActive,
        inputActive: inputActive, setInputActive: setInputActive,
        decklinkActive: decklinkActive,
        setDecklinkActive: setDecklinkActive
    } : SidebarProps) => {

    const changeScreen = (e: React.MouseEvent<HTMLButtonElement>) => {
        setInputActive(false);
        setDecklinkActive(false);
        switch(e.currentTarget.id) {
            case 'Input':
                setInputActive(true);
                break;
            case 'DecklinkOutput':
                setDecklinkActive(true);
                break;
            case "Play":
                setTransmit(true)
                setVideoActive(true)
        }
    };

    return (
        <div className='Sidebar'>
            <button onClick={changeScreen} id="Play" className="Sidebar_Button">Recorder Settings</button>
            <button onClick={changeScreen} id="Input" className={`Sidebar_Button ${inputActive && "Active_Button"}`}>Input</button>
            <button onClick={changeScreen} id="DecklinkOutput" className={`Sidebar_Button ${decklinkActive && "Active_Button"}`}>Decklink Output</button>
        </div>
    )
}