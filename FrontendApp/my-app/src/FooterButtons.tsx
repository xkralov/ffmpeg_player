import React from 'react'
import axios from 'axios'
import './App.css'

interface SidebarProps {
    isTransmit: boolean
    videoSource: string
    videoCodec: string
    videoFormat: string

    audioSource: string
    audioFormat: string

    outputCodec: string
    preset: string
    tune: string

    protocol: string 
    port: number
    url: string
    defaultSrt: boolean
    srtSendOptions: string
    srtRecvOptions: string
    isServer: boolean
    username: string
    password: string

    outputToDecklink: boolean
    decklinkOutput: string
}

export const FooterButtons = (
    {   
        isTransmit: isTransmit, videoSource: videoSource, 
        videoCodec: videoCodec, videoFormat: videoFormat,

        audioSource: audioSource, audioFormat: audioFormat,

        outputCodec: outputCodec, preset: preset, tune: tune,
        protocol: protocol, port: port, url: url, 
        defaultSrt: defaultSrt, srtSendOptions: srtSendOptions, 
        srtRecvOptions: srtRecvOptions, isServer: isServer,
        username: username, password: password,

        outputToDecklink: outputToDecklink, decklinkOutput: decklinkOutput
    } : SidebarProps) => {

    const start = () => {
        if (isTransmit) {
            startTransmission()
        } else if (!outputToDecklink) {
            startPlaying()
        } else {
            sendToDecklink()
        }
    }
    
    const startTransmission = async () => {
        await axios.post(
            'http://localhost:55555/record',
            {
                video: {
                    source: videoSource,
                    codec: videoCodec,
                    format: videoFormat
                },
                audio: {
                    source: audioSource,
                    format: audioFormat
                },
                codec: {
                    outputCodec: outputCodec,
                    preset: preset,
                    tune: tune
                },
                output: {
                    protocol: protocol, 
                    port: port, 
                    url: url, 
                    defaultSrt: defaultSrt, 
                    srtOptions: srtSendOptions,
                    isServer: isServer,
                    username: username,
                    password: password,
                }
            }
        )
    }

    const startPlaying = async () => {
        await axios.post(
            'http://localhost:55555/play',
            {
                port: port, 
                url: url, 
                defaultSrt: defaultSrt, 
                srtOptions: srtRecvOptions,
                isServer: isServer,
            }
        )
    }

    const sendToDecklink = async () => {
        await axios.post(
            'http://localhost:55555/decklink',
            {
                port: port, 
                url: url, 
                defaultSrt: defaultSrt, 
                srtOptions: srtRecvOptions,
                isServer: isServer,
                device: decklinkOutput
            }
        )
    }

    const stop = async () => {
        await axios.post(
            'http://localhost:55555/stop',
        )
    }

    const restart = async () => {
        await axios.post(
            'http://localhost:55555/restart',
        )
    }

    return (
        <div>
            <div className='Submit_Button'>
                <button onClick={start} className='Sidebar_Button Start_Button'>Start</button>
                <button onClick={stop} className='Sidebar_Button Stop_Button'>Stop</button>
                <button onClick={restart} className='Sidebar_Button Restart_Button'>Restart</button>
            </div>
        </div>
    )
}