import React, { useEffect } from "react";

interface OutputProps {
    port: number;
    setPort: React.Dispatch<React.SetStateAction<number>>;
    url: string;
    setUrl: React.Dispatch<React.SetStateAction<string>>;
    defaultSrt: boolean
    setDefaultSrt: React.Dispatch<React.SetStateAction<boolean>>;
    srtOptions: string;
    setSrtOptions: React.Dispatch<React.SetStateAction<string>>;
    isServer: boolean
    setIsServer: React.Dispatch<React.SetStateAction<boolean>>;
}

export const InputSettingScreen = (
    {
        port: port, setPort: setPort,
        url: url, setUrl: setUrl,
        defaultSrt: defaultSrt, setDefaultSrt: setDefaultSrt,
        srtOptions: srtOptions, setSrtOptions: setSrtOptions,
        isServer: isServer, setIsServer: setIsServer,
    }: OutputProps) => {
    
    return (
        <div className='Setting_Screen'>
            Input Settings
            <div className="Output_Screen">

                <div className="Url">
                    <p className="Setting_Screen_Item">URL</p>
                    <input 
                        type="text"
                        className="Setting_Screen_Item" 
                        onChange={event => setUrl(event.target.value)}
                        value={url}
                        disabled={isServer}
                        list="outputs"
                    />
                    <p className="Setting_Screen_Item">Port</p>
                    <input 
                        type="number"
                        className="Setting_Screen_Item" 
                        onChange={event => setPort(parseInt(event.target.value))}
                        value={port}
                    />
                </div>

                <div style={{marginLeft: "2rem"}}>

                <div className="Protocol_Options">
                    <p><input
                        type="checkbox" 
                        className="Setting_Screen_Item"
                        defaultChecked={isServer} 
                        onChange={event => setIsServer(event.target.checked)} />
                        Server
                    </p>  
                    <p><input
                        type="checkbox" 
                        className="Setting_Screen_Item"
                        defaultChecked={defaultSrt} 
                        onChange={event => setDefaultSrt(event.target.checked)} />
                        Default latency
                    </p>                
                    { defaultSrt || 
                        <><p className="Setting_Screen_Item">Srt Options</p>
                        <textarea
                            className="Setting_Screen_Item" 
                            onChange={event => setSrtOptions(event.target.value)}
                            value={srtOptions}
                            rows={4}
                            cols={30}
                        /></>}
                </div>
                </div>
            </div>
        </div>
    )
}