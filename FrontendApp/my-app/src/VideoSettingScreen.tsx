import { useEffect} from "react";

interface VideoProps {
    videoSource: string;
    setVideoSource: React.Dispatch<React.SetStateAction<string>>;
    videoCodec: string;
    setVideoCodec: React.Dispatch<React.SetStateAction<string>>;
    videoFormat: string;
    setVideoFormat: React.Dispatch<React.SetStateAction<string>>;
    devices: Array<AvailableDevices>;
}

interface AvailableDevices {
    name: string;
    formats: Array<string>;
    sizes: Array<string>;
    type: string;
  }

export const VideoSettingScreen = (
    {
        videoSource: source, setVideoSource: setSource,
        videoCodec: codec, setVideoCodec: setCodec,
        videoFormat: format, setVideoFormat: setFormat,
        devices: devices,
    } : VideoProps ) => {
    console.log(source)

    const sourceChanged = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setSource(event.target.value)
        devices.forEach(device => {
            if (device.name == event.target.value) {
                setFormat(device.type)
            }
        })
    }

    return (
        <div className='Setting_Screen'>
            <div>Video Source
                <p className="Setting_Screen_Item">Device (-i)</p>
                <select 
                    className="Setting_Screen_Item" 
                    defaultValue={source} 
                    onChange={sourceChanged}>
                        {
                            devices.map((device) => (
                                <option value={device.name} key={device.name} >{device.name}</option>
                            ))
                        }
                </select>
                <p className="Setting_Screen_Item">Input Codec (-input_format)</p>
                <input 
                    type="text"
                    className="Setting_Screen_Item" 
                    onChange={event => setCodec(event.target.value)} 
                    value={codec}
                    list="codecs"
                />
                <datalist id="codecs">
                    <option value="mjpeg" />
                    <option value="yuyv422" />
                </datalist>
                <p className="Setting_Screen_Item">Input Format (-f)</p>
                <input 
                    type="text"
                    className="Setting_Screen_Item" 
                    onChange={event => setFormat(event.target.value)} 
                    value={format}
                />
            </div>
        </div>
    )
}