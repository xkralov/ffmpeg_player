import React, { useEffect } from "react";

interface OutputProps {
    decklinkOutput: string
    setDecklinkOutput: React.Dispatch<React.SetStateAction<string>>
    outputToDecklink: boolean
    setOutputToDecklink: React.Dispatch<React.SetStateAction<boolean>>
    devices: Array<AvailableDevices>
}

interface AvailableDevices {
    name: string;
    formats: Array<string>;
    sizes: Array<string>;
    type: string;
  }

export const DecklinkOutputSettingScreen = (
    {
        decklinkOutput: decklinkOutput,
        setDecklinkOutput: setDecklinkOutput,
        outputToDecklink: outputToDecklink,
        setOutputToDecklink: setOutputToDecklink,
        devices: devices,
    }: OutputProps) => {

    return (
        <div className='Setting_Screen'>
            Output To Blackmagic
                <p className="Setting_Screen_Item">Device</p>
                <select 
                    className="Setting_Screen_Item" 
                    defaultValue={decklinkOutput} 
                    onChange={event => setDecklinkOutput(event.target.value)}>
                        {
                            devices.map((device) => (
                                <option value={device.name} key={device.name}>{device.name}</option>
                            ))
                        }
                </select>
                <p><input
                        type="checkbox" 
                        className="Setting_Screen_Item"
                        defaultChecked={outputToDecklink} 
                        onChange={event => setOutputToDecklink(event.target.checked)} />
                        Use Blackmagic
                    </p>
        </div>
    )
}