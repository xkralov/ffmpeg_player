import React from 'react'
import axios from 'axios'
import './App.css'

interface SidebarProps {
    setTransmit: React.Dispatch<React.SetStateAction<boolean>>
    videoActive: boolean;
    setVideoActive: React.Dispatch<React.SetStateAction<boolean>>;
    audioActive: boolean;
    setAudioActive: React.Dispatch<React.SetStateAction<boolean>>;
    codecActive: boolean;
    setCodecActive: React.Dispatch<React.SetStateAction<boolean>>;
    outputActive: boolean;
    setOutputActive: React.Dispatch<React.SetStateAction<boolean>>;
    setInputActive: React.Dispatch<React.SetStateAction<boolean>>;
}

export const Sidebar = (
    {   
        setTransmit: setTransmit,
        videoActive: videoActive, setVideoActive: setVideoActive,
        audioActive: audioActive, setAudioActive: setAudioActive,
        codecActive: codecActive, setCodecActive: setCodecActive,
        outputActive: outputActive, setOutputActive: setOutputActive,
        setInputActive: setInputActive
    } : SidebarProps) => {

    const changeScreen = (e: React.MouseEvent<HTMLButtonElement>) => {
        setVideoActive(false);
        setAudioActive(false);
        setCodecActive(false);
        setOutputActive(false);
        switch(e.currentTarget.id) {
            case "Video":
                setVideoActive(true);
                break;
            case "Audio":
                setAudioActive(true);
                break;
            case "Codec":
                setCodecActive(true);
                break;
            case "Output":
                setOutputActive(true);
                break;
            case "Play":
                setTransmit(false);
                setInputActive(true);
                break;
        }
    };

    return (
        <div className='Sidebar'>
            <button onClick={changeScreen} id="Play" className="Sidebar_Button">Player Settings</button>
            <button onClick={changeScreen} id="Video" className={`Sidebar_Button ${videoActive && "Active_Button"}`}>Video</button>
            <button onClick={changeScreen} id="Audio" className={`Sidebar_Button ${audioActive && "Active_Button"}`}>Audio</button>
            <button onClick={changeScreen} id="Codec" className={`Sidebar_Button ${codecActive && "Active_Button"}`}>Codec</button>
            <button onClick={changeScreen} id="Output" className={`Sidebar_Button ${outputActive && "Active_Button"}`}>Output</button>
        </div>
    )
}