
interface CodecProps {
    outputCodec: string;
    setOutputCodec: React.Dispatch<React.SetStateAction<string>>;
    preset: string;
    setPreset: React.Dispatch<React.SetStateAction<string>>;
    tune: string;
    setTune: React.Dispatch<React.SetStateAction<string>>;
}

export const CodecSettingScreen = (
    {
        outputCodec: codec, setOutputCodec: setCodec,
        preset: preset, setPreset: setPreset,
        tune: tune, setTune: setTune
    }: CodecProps) => {

    return (
        <div className='Setting_Screen'>
            <div>Output Codec Settings
                <p className="Setting_Screen_Item">Codec</p>
                <select
                    className="Setting_Screen_Item"
                    defaultValue={codec}
                    onChange={event => setCodec(event.target.value)}>
                        <option value="libx264">H.264</option>
                </select>
                <p className="Setting_Screen_Item">Preset</p>
                <select 
                    className="Setting_Screen_Item" 
                    defaultValue={preset} 
                    onChange={event => setPreset(event.target.value)}>
                        <option value="ultrafast">Ultra fast</option>
                        <option value="superfast">Super fast</option>
                        <option value="veryfast">Very fast</option>
                        <option value="fast">Fast</option>
                        <option value="medium">Medium</option>
                        <option value="slow">Slow</option>
                        <option value="slower">Slower</option>
                        <option value="veryslow">Very slow</option>
                </select>
                <p className="Setting_Screen_Item">Tune</p>
                <select 
                    className="Setting_Screen_Item" 
                    defaultValue={tune}
                    onChange={event => setTune(event.target.value)}>
                        <option value="film">Film</option>
                        <option value="animation">Animation</option>
                        <option value="grain">Grain</option>
                        <option value="stillimage">Stillimage</option>
                        <option value="fastdecode">Fastdecode</option>
                        <option value="zerolatency">Zerolatency</option>
                </select>
            </div>
        </div>
    )
}