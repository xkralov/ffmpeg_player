import { useEffect} from "react";

interface AudioProps {
    audioSource: string
    setAudioSource: React.Dispatch<React.SetStateAction<string>>;
    audioFormat: string
    setAudioFormat: React.Dispatch<React.SetStateAction<string>>;
    devices: Array<AvailableDevices>;
}

interface AvailableDevices {
    name: string;
    device_number: string;
  }

export const AudioSettingScreen = (
    {
        audioSource: source, setAudioSource: setSource,
        audioFormat: format, setAudioFormat: setFormat,
        devices: devices
    } : AudioProps ) => {

    return (
        <div className='Setting_Screen'>
            <div>Audio Source
                <p className="Setting_Screen_Item">Device (-i)</p>
                <select 
                    className="Setting_Screen_Item" 
                    defaultValue={source} 
                    onChange={event => setSource(event.target.value)}>
                        {
                            devices.map((device) => (
                                <option value={device.device_number} key={device.device_number}>{device.name}</option>
                            ))
                        }
                </select>
                <p className="Setting_Screen_Item">Input Format (-f)</p>
                <input 
                    type="text"
                    className="Setting_Screen_Item" 
                    onChange={event => setFormat(event.target.value)}
                    value={format}
                />
            </div>
        </div>
    )
}