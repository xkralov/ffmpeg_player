import React, { useEffect } from "react";

interface OutputProps {
    protocol: string;
    setProtocol: React.Dispatch<React.SetStateAction<string>>;
    port: number;
    setPort: React.Dispatch<React.SetStateAction<number>>;
    url: string;
    setUrl: React.Dispatch<React.SetStateAction<string>>;
    defaultSrt: boolean
    setDefaultSrt: React.Dispatch<React.SetStateAction<boolean>>;
    srtOptions: string;
    setSrtOptions: React.Dispatch<React.SetStateAction<string>>;
    usingSrt: boolean
    setUsingSrt: React.Dispatch<React.SetStateAction<boolean>>;
    isServer: boolean;
    setIsServer: React.Dispatch<React.SetStateAction<boolean>>;
    username: string;
    setUsername: React.Dispatch<React.SetStateAction<string>>;
    password: string;
    setPassword: React.Dispatch<React.SetStateAction<string>>;
}

export const OutputSettingScreen = (
    {
        protocol: protocol, setProtocol: setProtocol,
        port: port, setPort: setPort,
        url: url, setUrl: setUrl,
        defaultSrt: defaultSrt, setDefaultSrt: setDefaultSrt,
        srtOptions: srtOptions, setSrtOptions: setSrtOptions,
        usingSrt: usingSrt, setUsingSrt: setUsingSrt,
        isServer: isServer, setIsServer: setIsServer,
        username: username, setUsername: setUsername,
        password: password, setPassword: setPassword,
    }: OutputProps) => {
    
    useEffect( () => {
        if (protocol == 'srt') {
            setUsingSrt(true)
        } else {
            setUsingSrt(false)
        }
    }, [protocol])
    
    return (
        <div className='Setting_Screen'>
            Output Settings
            <div className="Output_Screen">

                <div className="Url">
                    <p className="Setting_Screen_Item">URL</p>
                    <input 
                        type="text"
                        className="Setting_Screen_Item" 
                        onChange={event => setUrl(event.target.value)}
                        value={url}
                        disabled={isServer}
                    />
                    { usingSrt &&
                        <><p className="Setting_Screen_Item">Port</p>
                        <input 
                            type="number"
                            className="Setting_Screen_Item" 
                            onChange={event => setPort(parseInt(event.target.value))}
                            value={port}
                        /></>
                    }

                    { !usingSrt &&
                    <div className="Protocol_Options">
                        <p className="Setting_Screen_Item">Username</p>
                        <input 
                            type="text"
                            className="Setting_Screen_Item" 
                            onChange={event => setUsername(event.target.value)}
                            value={username}
                        />
                        <p className="Setting_Screen_Item">Password</p>
                        <input 
                            type="password"
                            className="Setting_Screen_Item" 
                            onChange={event => setPassword(event.target.value)}
                            value={password}
                        />
                    </div> }
                </div>

                <div style={{marginLeft: "2rem"}}>
                <div className="Protocol_Form">
                    <p className="Setting_Screen_Item">Protocol</p>
                    <select 
                        className="Setting_Screen_Item" 
                        defaultValue={protocol} 
                        onChange={event => setProtocol(event.target.value)}>
                            <option value="srt">SRT</option>
                            <option value="rtmp">RTMP</option>
                    </select>
                </div>

                { usingSrt &&
                <div className="Protocol_Options">
                    <p><input
                        type="checkbox" 
                        className="Setting_Screen_Item"
                        defaultChecked={isServer} 
                        onChange={event => setIsServer(event.target.checked)} />
                        Server
                    </p>   
                    <p><input
                        type="checkbox" 
                        className="Setting_Screen_Item"
                        defaultChecked={defaultSrt} 
                        onChange={event => setDefaultSrt(event.target.checked)} />
                        Default latency
                    </p>                
                    { defaultSrt || 
                        <><p className="Setting_Screen_Item">Srt Options</p>
                        <textarea
                            className="Setting_Screen_Item" 
                            onChange={event => setSrtOptions(event.target.value)}
                            value={srtOptions}
                            rows={4}
                            cols={30}
                        /></>}
                </div> }

                </div>

            </div>
        </div>
    )
}