import React, {useEffect, useState} from 'react';
import axios from 'axios'
import './App.css';
import { AudioSettingScreen } from './AudioSettingScreen';
import { CodecSettingScreen } from './CodecSettingScreen';
import { OutputSettingScreen } from './OutputSettingScreen';
import { Sidebar } from './Sidebar';
import { VideoSettingScreen } from './VideoSettingScreen';
import { SidebarPlayer } from './SidebarPlayer';
import { InputSettingScreen } from './InputSettingScreen';
import { FooterButtons } from './FooterButtons';
import { DecklinkOutputSettingScreen } from './DecklinkOutput';

interface AvailableVideoDevices {
  name: string;
  formats: Array<string>;
  sizes: Array<string>;
  type: string;
}

interface AvailableAudioDevices {
  name: string;
  device_number: string;
}

interface AvailableOutputDevices {
  name: string;
  formats: Array<string>;
  sizes: Array<string>;
  type: string;
}

function App() {

  const [transmit, setTransmit] = useState(true)
  
  const [videoActive, setVideoActive] = useState(true)
  const [audioActive, setAudioActive] = useState(false)
  const [codecActive, setCodecActive] = useState(false)
  const [outputActive, setOutputActive] = useState(false)
  const [inputActive, setInputActive] = useState(false)
  const [decklinkActive, setDecklinkActive] = useState(false)

  const [videoSource, setVideoSource] = useState('')
  const [videoCodec, setVideoCodec] = useState('')
  const [videoFormat, setVideoFormat] = useState('v4l2')
  const [videoDevices, setVideoDevices] = useState<Array<AvailableVideoDevices>>([])

  const [audioSource, setAudioSource] = useState('')
  const [audioFormat, setAudioFormat] = useState('alsa')
  const [audioDevices, setAudioDevices] = useState<Array<AvailableAudioDevices>>([])

  const [outputCodec, setOutputCodec] = useState('libx264')
  const [preset, setPreset] = useState('ultrafast')
  const [tune, setTune] = useState('zerolatency')

  const [protocol, setProtocol] = useState('srt')
  const [port, setPort] = useState(12345)
  const [url, setUrl] = useState('localhost')
  const [defaultSrt, setDefaultSrt] = useState(true)
  const [srtSendOptions, setSrtSendOptions] = useState('pkt_size=1316&smoother=live&transtype=live&send_buffer_size=0&latency=0')
  const [srtRecvOptions, setSrtRecvOptions] = useState('pkt_size=1316&smoother=live&transtype=live&recv_buffer_size=0&latency=0')
  const [usingSrt, setUsingSrt] = useState(true)
  const [isServer, setIsServer] = useState(false)
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const [decklinkOutput, setDecklinkOutput] = useState('')
  const [outputToDecklink, setOutputToDecklink] = useState(false)
  const [outputDevices, setOutputDevices] = useState<Array<AvailableOutputDevices>>([])


  useEffect( () => {
    axios.get(
      'http://localhost:55555/devices'
      ).then( (response) => {
        setVideoDevices(response.data.video_devices)
        setAudioDevices(response.data.audio_devices)
        setOutputDevices(response.data.output_devices)
        setVideoSource(response.data.video_devices[0]?.name ?? '')
        setVideoCodec(response.data.video_devices[0]?.formats[0] ?? '')
        setAudioSource(response.data.audio_devices[0]?.device_number ?? '')
        setDecklinkOutput(response.data.output_devices[0]?.name ?? '')
      })
  }, [])

  return (
    <>
    { transmit && <h2 style={{color: 'white', textAlign: 'center'}}>Recorder Settings</h2> }
    { !transmit && <h2 style={{color: 'white', textAlign: 'center'}}>Player Settings</h2> }
    { transmit && <div className="App">
      <Sidebar 
        setTransmit={setTransmit}
        videoActive={videoActive} setVideoActive={setVideoActive}
        audioActive={audioActive} setAudioActive={setAudioActive}
        codecActive={codecActive} setCodecActive={setCodecActive}
        outputActive={outputActive} setOutputActive={setOutputActive}
        setInputActive={setInputActive}
      />
      {videoActive && <VideoSettingScreen 
        videoSource={videoSource} setVideoSource={setVideoSource}
        videoCodec={videoCodec} setVideoCodec={setVideoCodec}
        videoFormat={videoFormat} setVideoFormat={setVideoFormat}
        devices={videoDevices}
      /> }
      {audioActive && <AudioSettingScreen
        audioSource={audioSource} setAudioSource={setAudioSource}
        audioFormat={audioFormat} setAudioFormat={setAudioFormat}
        devices={audioDevices}
      /> }
      {codecActive && <CodecSettingScreen
        outputCodec={outputCodec} setOutputCodec={setOutputCodec}
        preset={preset} setPreset={setPreset}
        tune={tune} setTune={setTune}
      />}
      {outputActive && <OutputSettingScreen
        protocol={protocol} setProtocol={setProtocol}
        port={port} setPort={setPort}
        url={url} setUrl={setUrl}
        defaultSrt={defaultSrt} setDefaultSrt={setDefaultSrt}
        srtOptions={srtSendOptions} setSrtOptions={setSrtSendOptions}
        usingSrt={usingSrt} setUsingSrt={setUsingSrt}
        isServer={isServer} setIsServer={setIsServer}
        username={username} setUsername={setUsername}
        password={password} setPassword={setPassword}
      />}
    </div> }

    { !transmit && <div className="App">
      <SidebarPlayer
        setTransmit={setTransmit} 
        setVideoActive={setVideoActive}
        inputActive={inputActive} setInputActive={setInputActive}
        decklinkActive={decklinkActive} 
        setDecklinkActive={setDecklinkActive}
      />
      {inputActive && <InputSettingScreen
        port={port} setPort={setPort}
        url={url} setUrl={setUrl}
        defaultSrt={defaultSrt} setDefaultSrt={setDefaultSrt}
        srtOptions={srtRecvOptions} setSrtOptions={setSrtRecvOptions}
        isServer={isServer} setIsServer={setIsServer}
      />}
      {decklinkActive && <DecklinkOutputSettingScreen
        decklinkOutput={decklinkOutput}
        setDecklinkOutput={setDecklinkOutput}
        outputToDecklink={outputToDecklink}
        setOutputToDecklink={setOutputToDecklink}
        devices={outputDevices}
      />}
    </div> }
    <footer>
      <FooterButtons
        isTransmit={transmit} videoSource={videoSource}
        videoCodec={videoCodec} videoFormat={videoFormat}
        audioSource={audioSource} audioFormat={audioFormat}
        outputCodec={outputCodec} preset={preset} tune={tune}
        protocol={protocol} port={port} url={url} 
        defaultSrt={defaultSrt} srtSendOptions={srtSendOptions} 
        srtRecvOptions={srtRecvOptions} isServer={isServer}
        outputToDecklink={outputToDecklink} decklinkOutput={decklinkOutput}
        username={username} password={password}
      />
    </footer>
    </>
  );
}

export default App;
