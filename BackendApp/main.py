from typing import List
from flask import Flask, request
from flask_cors import CORS
from subprocess import Popen
import time
import ffmpeg

HOST = ''
PORT = 55555
app = Flask(__name__)
CORS(app)
FFMPEG_PROCESS: List[Popen[bytes] | Popen[str | bytes]] = []


@app.route('/record', methods=['POST'])
def start_transmission():
    data = request.get_json()
    global FFMPEG_PROCESS
    FFMPEG_PROCESS.append(ffmpeg.prepare_transmission(data))
    return 'Success'


@app.route('/play', methods=['POST'])
def start_playing():
    data = request.get_json()
    global FFMPEG_PROCESS
    FFMPEG_PROCESS.append(ffmpeg.prepare_playing(data))
    return 'Success'


@app.route('/decklink', methods=['POST'])
def play_on_blackmagic():
    data = request.get_json()
    global FFMPEG_PROCESS
    FFMPEG_PROCESS.append(ffmpeg.prepare_decklink(data))
    return 'Success'


@app.route('/stop', methods=['POST'])
def stop():
    for process in FFMPEG_PROCESS:
        process.kill()
    FFMPEG_PROCESS.clear()
    return "Success"


@app.route('/restart', methods=['POST'])
def restart():
    for process in FFMPEG_PROCESS:
        process.kill()
    time.sleep(2)
    for process_index in range(len(FFMPEG_PROCESS)):
        process = FFMPEG_PROCESS[process_index]
        script = process.args
        FFMPEG_PROCESS[process_index] = ffmpeg.start(script)
    return "Success"


@app.route('/devices', methods=['GET'])
def devices():
    devices_json = {
        'video_devices': ffmpeg.get_video_devices(),
        'audio_devices': ffmpeg.get_audio_devices(),
        'output_devices': ffmpeg.get_output_devices()
    }
    return devices_json


app.run(port=PORT)
