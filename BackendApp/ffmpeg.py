import subprocess


def prepare_transmission(data):
    video = data['video']
    audio = data['audio']

    if not (video['source'] or audio['source']):
        raise ValueError

    script = ['ffmpeg', '-hide_banner']
    if video['source']:
        script += ['-thread_queue_size', '4096', '-probesize', '32', '-analyzeduration', '0']
        if video['format']:
            script += ['-f', video['format']]

        if video['codec']:
            script += ['-input_format', video['codec']]

        script += ['-i', video['source']]

    if audio['source']:
        script += ['-thread_queue_size', '4096', '-probesize', '32', '-analyzeduration', '0']
        if audio['format']:
            script += ['-f', audio['format']]
        audio_source = 'hw:' + audio['source']
        script += ['-i', audio_source]

    codec = data['codec']
    if codec['outputCodec'] == 'libx264':
        script += ['-vcodec', codec['outputCodec']]

        if codec['preset']:
            script += ['-preset', codec['preset']]

        if codec['tune']:
            script += ['-tune', codec['tune']]

    script += ['-acodec', 'aac']

    output = data['output']

    if output['protocol'] == 'srt':
        script += ['-f', 'mpegts']
    elif output['protocol'] == 'rtmp':
        script += ['-f', 'flv']

    url = create_url(output['protocol'], output['url'], output['port'], output['srtOptions'],
                     output['defaultSrt'], output['isServer'], True)

    script.append(url)
    return start(script)


def prepare_playing(data):

    script = ['ffplay', '-probesize', '32', '-analyzeduration', '0',
              '-sync', 'ext', '-fflags', 'nobuffer', '-flags', 'low_delay',
              '-framedrop', '-strict', 'experimental']
    url = create_url('srt', data['url'], data['port'], data['srtOptions'],
                     data['defaultSrt'], data['isServer'], False)
    script.append(url)
    return start(script)


def prepare_decklink(data):
    if not data['device']:
        raise ValueError

    url = create_url('srt', data['url'], data['port'], data['srtOptions'],
                     data['defaultSrt'], data['isServer'], False)
    script = ['ffmpeg', '-thread_queue_size', '4096', '-probesize', '32', '-analyzeduration', '0',
              '-i', url, data['device']]
    return start(script)


def create_url(protocol, url, port, options, default, server, sender, user=None, password=None):
    if default:
        if sender:
            options = 'pkt_size=1316&smoother=live&transtype=live&send_buffer_size=0&latency=0'
        else:
            options = 'pkt_size=1316&smoother=live&transtype=live&recv_buffer_size=0&latency=0'

    if server:
        if options:
            options += '&mode=listener'
        else:
            options = 'mode=listener'
        url = '0.0.0.0'

    if protocol == 'srt':
        url = protocol + '://' + url + ':' + str(port) + '?' + options
    elif protocol == 'rtmp':
        original_url = url
        url = protocol + '://'
        if user and password:
            url += user + ':' + password + '@'
        url += original_url
    else:
        raise ValueError

    return url


def start(script):
    ffmpeg_process = subprocess.Popen(script)
    return ffmpeg_process


def get_video_devices():
    result_devices = parse_v4l2_devices()
    result_devices += parse_decklink_devices()

    return result_devices


def parse_v4l2_devices():
    v4l2_devices = subprocess.run(['ffmpeg', '-hide_banner', '-sources', 'v4l2'],
                                  stdout=subprocess.PIPE).stdout

    result_devices = []
    devices_map = {}
    for v4l2_device in str(v4l2_devices).split('\\n'):
        if v4l2_device.startswith(' '):
            v4l2_device = v4l2_device.strip()
            split_device = v4l2_device.split(' ', 1)
            devices_map[split_device[1]] = split_device[0]

    v4l2_cleaned = [device for device in reversed(devices_map.values())]
    for device in v4l2_cleaned:
        formats = subprocess.run(
            ['ffmpeg', '-hide_banner', '-f', 'v4l2',
             '-list_formats', 'all', '-i', device],
            stderr=subprocess.PIPE)
        device_info = str(formats.stderr)
        video_formats = []
        video_sizes = []
        for one_format_info in device_info.split('\\n'):
            split_info = one_format_info.split(': ')
            if len(split_info) >= 4:
                video_formats.append(split_info[1].strip())
                video_sizes.append(split_info[3].strip().split())
        result_devices.append(
            {'name': device, 'formats': video_formats, 'sizes': video_sizes, 'type': 'v4l2'})
    return result_devices


def parse_decklink_devices():
    blackmagic_devices = subprocess.run(['ffmpeg', '-hide_banner', '-sources', 'decklink'],
                                        stdout=subprocess.PIPE).stdout

    result_devices = []
    blackmagic_cleaned = []
    for blackmagic_device in str(blackmagic_devices).split('\\n'):
        if blackmagic_device.startswith(' '):
            blackmagic_device = blackmagic_device.split('[')[1]
            blackmagic_device = blackmagic_device.split(']')[0]
            blackmagic_cleaned.append(blackmagic_device)

    for device in blackmagic_cleaned:
        formats = subprocess.run(
            ['ffmpeg', '-hide_banner', '-f', 'decklink',
             '-list_formats', '1', '-i', device],
            stderr=subprocess.PIPE)
        device_info = str(formats.stderr)
        for one_format_info in device_info.split('\\n'):
            print('Parse input and get formats')
        result_devices.append(
            {'name': device, 'formats': [], 'sizes': [], 'type': 'decklink'}
        )

    return result_devices


def get_audio_devices():
    found_devices = subprocess.check_output(['arecord', '-l'])
    devices_cleaned = []
    for device in str(found_devices).split('\\n'):
        if device.startswith('card'):
            device_list = device.split(' ', 3)
            device_name = device_list[3].split(']')[0][1:]
            devices_cleaned.append({'device_number': device_list[1][:-1], 'name': device_name})

    return devices_cleaned


def get_output_devices():
    blackmagic_devices = subprocess.run(['ffmpeg', '-hide_banner', '-sinks', 'decklink'],
                                        stdout=subprocess.PIPE).stdout

    result_devices = []
    blackmagic_cleaned = []
    for blackmagic_device in str(blackmagic_devices).split('\\n'):
        if blackmagic_device.startswith(' '):
            blackmagic_device = blackmagic_device.split('[')[1]
            blackmagic_device = blackmagic_device.split(']')[0]
            blackmagic_cleaned.append(blackmagic_device)

    for device in blackmagic_cleaned:
        formats = subprocess.run(
            ['ffmpeg', '-hide_banner', '-f', 'decklink',
             '-list_formats', '1', '-i', device],
            stderr=subprocess.PIPE)
        device_info = str(formats.stderr)
        for one_format_info in device_info.split('\\n'):
            print('Parse input and get formats')
        result_devices.append(
            {'name': device, 'formats': [], 'sizes': [], 'type': 'decklink'}
        )

    return result_devices
