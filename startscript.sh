#!/bin/sh -e

cd FrontendApp/my-app
npm start &
NPM_PID=$!

cd ../../BackendApp
python3 main.py
kill $NPM_PID